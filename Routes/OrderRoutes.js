const express = require('express');
const router = express.Router();
// const db = require('../Service/DatabaseService');
const Order = require('../Model/order');
const Product = require('../Model/product');
// const Sequelize = require('sequelize');

router.use(express.json());

// Get all orders
router.get('/', (req, res) => {
    Order.findAll().then(orders => {
        res.json(orders);
    }).catch(err => {
        res.json(err);
    })
});

// Get order by id
router.get('/:id', (req, res) => {
    Order.findOne({
        where: {
            orderId: req.params.id
        }
    }).then(order => {
        if (order) {
            res.json({ order: order });
        } else {
            res.statusCode = 404;
            res.json({ message: 'Order not found' });
        }
    }).catch(err => {
        res.statusCode = 400;
        res.json({ errors: err });
    })
});

function updateProduct(stock, body) {
    return Product.update({
        stock: stock
    }, {
        where: {
            productID: body.productID
        }
    });
}

function findProduct(body) {
    return Product.findOne({
        where: {
            productID: body.productID
        }
    });
}


// Add new order
router.post("/add", (req, res) => {

    let body = req.body;
    body.status = "placed";

    findProduct(body).then(product => {
        const stock = product.stock - body.quantity;
        updateProduct(stock, body).then(() => {

            Order.create(body).then(order => {
                res.statusCode = 201;
                res.json({ "orderID": order.orderId });
            }).catch(err => {
                res.statusCode = 400;
                res.json({ "errors": err });
            })
        }).catch(err => {
            res.statusCode = 400;
            res.json({ "errors": "Requested quantity is not available" });
        })
    }).catch(err => {
        res.statusCode = 400;
        res.json({ "errors": "Product not found" });
    })
})

// Update order by id
router.put("/update/:id", (req, res) => {
    let id = req.params.id;
    let body = req.body;

    // Check if order exists
    Order.findOne({
        where: {
            orderId: id
        }
    }).then(order => {
        const quantity = body.quantity - order.quantity;
        const productID = order.productID;

        findProduct({ "productID": productID }).then(product => {

            updateProduct(product.stock - quantity, { productID: productID }).then(() => {
                Order.update(body, {
                    where: {
                        orderId: id
                    },
                    set: {
                        quantity: body.quantity,
                    }
                }).then(order => {
                    res.statusCode = 204;
                    res.json({ order: order });
                }).catch(err => {
                    res.json({ errors: err });
                })
            }).catch(err => {
                res.statusCode = 400;
                console.log(err);
                res.json({ errors: "Requested quantity is not available." });
            })
        })
    }).catch(err => {
        res.statusCode = 400;
        res.json({ errors: "Order not found" });
    })
})

// Update order status
router.put("/updateStatus/:id", (req, res) => {
    let id = req.params.id;
    let body = req.body;
    Order.update(body, {
        where: {
            orderId: id
        },
        set: {
            status: body.status
        }
    }).then(order => {
        res.statusCode = 204;
        res.json(order);
    }).catch(err => {
        res.json(err);
    })
})

// Delete order by id
router.delete("/delete/:id", (req, res) => {
    let id = req.params.id;
    Order.destroy({
        where: {
            orderId: id
        }
    }).then(order => {
        res.statusCode = 204;
        res.json(order);
    }).catch(err => {
        res.json(err);
    })
})

module.exports = router