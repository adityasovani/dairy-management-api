const express = require('express');
const router = express.Router();
// const db = require('../Service/DatabaseService');
const Product = require('../Model/product');
// const Sequelize = require('sequelize');

router.use(express.json());

// Set Product quantity
router.put("/update/:id", (req, res) => {
    let id = req.params.id;
    let body = req.body;
    Product.update(body, {
        where: {
            productID: id
        },
        set: {
            stock: body.quantity,
        }
    }).then(product => {
        res.statusCode = 204;
        res.json(product);
    }).catch(err => {
        res.json(err);
    })
})

// Get quantity of product by id
router.get("/quantity/:id", (req, res) => {
    let id = req.params.id;
    Product.findOne({
        where: {
            productID: id
        }
    }).then(product => {
        res.json(product);
    }).catch(err => {
        res.json(err);
    })
})

// Get all products
router.get('/', (req, res) => {
    Product.findAll().then(products => {
        res.json(products);
    }).catch(err => {
        res.json(err);
    })
})

// Add new product
router.post("/add", (req, res) => {
    let body = req.body;
    Product.create(body).then(product => {
        res.statusCode = 201;
        res.json(product.productID);
    }).catch(err => {
        res.json(err);
    })
})

module.exports = router