const Capacity = require('../Model/Capacity');
const express = require('express');
const router = express.Router();

router.use(express.json());

// Check capacity for given date
router.get('/checkCapacity/:date', (req, res) => {
    let date = req.params.date;
    Capacity.findOne({
        where: {
            date: date,
        }
    }).then(capacity => {
        res.json(capacity);
    }).catch(err => {
        res.json(err);
    })
});

module.exports = router;