const express = require('express');
const app = express();
const cron = require("node-cron");
const sequelize = require("./Service/DatabaseService");
const path = require('path');
const Product = require('./Model/product');
const Capacity = require('./Model/Capacity');

// Test connection to database
sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
}).catch(err => {
    console.error('Unable to connect to the database:', err);
});

app.get('/', (req, res) => {
    // res.json({ "message": "Hello Worlds" })
    res.sendFile(path.join(__dirname, '/Templates/index.html'));
})

// Routes
app.use('/orders', require('./Routes/OrderRoutes'));
app.use('/products', require('./Routes/ProductRoutes'));
app.use('/capacities', require('./Routes/CapacityRoutes'));

// Swagger documentation
const swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');
app.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument)
);

// Create cron job that runs everyday at midnight and  stores milk capacity of previus day in DB
cron.schedule("0 0 * * *", () => {
    console.log("Running Cron Job");
    // Get product by productID
    Product.findOne({
        where: {
            productID: 1
        }
    }).then(product => {
        // put yesterday's date in variable
        let date = new Date();
        date.setDate(date.getDate() - 1);
        date = date.toISOString().slice(0, 10);
        // Create object to store in DB
        let obj = {
            productID: product.productID,
            date: date,
            quantity: product.stock
        }
        // Create capacity entry in DB
        Capacity.create(obj).then(capacity => {
            console.log("Created capacity entry");
        }).catch(err => {
            console.log("Error creating capacity entry", err);
        })
    }).catch(err => {
        console.log(err);
    })
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log('Server started on port',PORT);
});