const Sequelize = require("sequelize");
const db = require("../Service/DatabaseService");

const Order = db.define("order", {
    orderId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    productID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: "products",
            key: "productID"
        }
    },
    quantity: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 1
        }
    },
    status: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isIn: [['placed', 'packed', 'dispathed', 'delivered']]
        }
    }
})

Order.sync().then().catch(err => {
    console.log("Error creating order table",err);
})

module.exports = Order;