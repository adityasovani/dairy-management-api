const Sequelize = require("sequelize");
const db = require("../Service/DatabaseService");

const Product = db.define("product", {
    productID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    SKU: {
        type: Sequelize.STRING,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    stock: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            min: 0
        }
    },
    price: {
        type: Sequelize.FLOAT,
        allowNull: false
    }
})

Product.sync().then().catch(err => {
    console.log("Error creating product table:",err);
})

module.exports = Product;