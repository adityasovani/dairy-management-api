const Sequelize = require("sequelize");
const db = require("../Service/DatabaseService");

const Capacity = db.define("capacity", {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    productID: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: "products",
            key: "productID"
        }
    },
    date: {
        type: Sequelize.STRING,
        allowNull: false
    },
    quantity: {
        type: Sequelize.INTEGER,
        allowNull: false
    }

})

Capacity.sync().then().catch(err => {
    console.log("Error creating capacity table",err);
})

module.exports = Capacity;
