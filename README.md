# Dairy Milk Distributor API

##### An API for Dairy written in Node.js, Express, and PostgreSQL  

###### Hosted on https://dairy-backend-app.herokuapp.com/
[![Heroku URL](https://img.shields.io/badge/heroku-%23430098.svg?style=for-the-badge&logo=heroku&logoColor=white)](https://dairy-backend-app.herokuapp.com/)

This is backend API for Dairy Management. Following operations are supported:
- List and Add orders
- Find order by ID
- Update order quantity, status
- View leftover product quantity by date.

## Features
- On start of every day, a cron job will run and update last day's leftover quantity in ```Capacity``` table
- Swagger API documentation (https://dairy-backend-app.herokuapp.com/api-docs).
- Hosted on Heroku (https://dairy-backend-app.herokuapp.com/)

# Built using
- Node.js, Expres.js
- Postgres
- Sequelize
- Swagger (documentation)
- Heroku
- Gitlab CI-CD
 
# Installation

Requires [Node.js](https://nodejs.org/) v10+ to run.
Install the dependencies and start the server.

```sh
cd dairy-management-api
npm install
npm run start
```
Server will run on http://localhost:3000/

In ```Service / DatabaseService.js``` put your postgress connection string.

__Replace above localhost URL with Heroku URL in order to run it without local installation.__

# Usage
#### List all orders:
```GET``` http://localhost:3000/orders
#### Find order by ID
```GET``` http://localhost:3000/orders/:id
#### Add order
```POST``` http://localhost:3000/orders/add
Sample payload:
```
{
    "quantity":15,
    "productID":1
}
```
#### Update order quantity
```PUT``` http://localhost:3000/orders/update/:id 
Sample payload:
```
{
    "quantity":15
}
```
#### Update order status
```PUT``` http://localhost:3000/orders/updateStatus/:id 
Sample payload (status must be 'placed', 'packed', 'dispathed' or 'delivered'):
```
{
    "status":"packed"
}
```
#### Delete order
```DELETE``` http://localhost:3000/orders/delete/:id

#### Update STOCK
```PUT``` http://localhost:3000/products/update/1
```
{
    "stock":750
}
```
#### List all products:
```GET``` http://localhost:3000/products
#### Add product
```POST``` http://localhost:3000/products/add
```
{
    "name":"Milk 250ml",
    "stock":1000
}
```

#### Check Capacity
```GET```  http://localhost:3000/products/checkCapacity/:date
The ```:date``` must be in **YYYY-MM-DD** format.